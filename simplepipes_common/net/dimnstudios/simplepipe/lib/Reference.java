package net.dimnstudios.simplepipe.lib;

public class Reference {
	
	public static final String MOD_ID = "simple_pipe";
	public static final String MOD_NAME = "Simple Pipe";
	public static final String MOD_VERSION = "%VERSION%";
	
	public static final String COMMON_PROXY_CLASS = "net.dimnstudios.simplepipe.CommonProxy";

}
